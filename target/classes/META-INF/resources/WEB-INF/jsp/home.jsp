<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta content="text/html;charset=UTF-8"/>
    <title authentication="name"></title>
    <link rel="stylesheet" href="css/bootstrap.min.css"/>
    <style type="text/css">
        body {
            padding-top: 50px;
        }

        .starter-template {
            padding: 40px 15px;
            text-align: center;
        }
    </style>
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Spring Security演示</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li><a href="@{/}"> 首页 </a></li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>


<div class="container">
    <div class="starter-template">
        <h1 text="${msg.title}"></h1>

        <p class="bg-primary" text="${msg.content}"></p>

        <div authorize="hasRole('ROLE_ADMIN')"> <!-- 用户类型为ROLE_ADMIN 显示 -->
            <p class="bg-info" text="${msg.etraInfo}"></p>
        </div>

        <div authorize="hasRole('ROLE_USER')"> <!-- 用户类型为 ROLE_USER 显示 -->
            <p class="bg-info">无更多信息显示</p>
        </div>

        <form action="/logout" method="post">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            <input type="submit" class="btn btn-primary" value="注销"/>
        </form>
    </div>

</div>
</body>
</html>
