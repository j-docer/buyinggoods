SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for t_product
-- ----------------------------
DROP TABLE IF EXISTS `t_product`;
CREATE TABLE `t_product` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `product_name` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `stock` int(12) DEFAULT NULL,
  `price` decimal(16,2) DEFAULT NULL,
  `version` int(10) DEFAULT NULL,
  `note` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='产品信息表';

-- ----------------------------
-- Records of t_product
-- ----------------------------
INSERT INTO `t_product` VALUES ('1', '飞机', '30000', '5.00', '0', '测试数据');

-- ----------------------------
-- Table structure for t_purchase_record
-- ----------------------------
DROP TABLE IF EXISTS `t_purchase_record`;
CREATE TABLE `t_purchase_record` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `user_id` int(12) DEFAULT NULL,
  `product_id` int(12) DEFAULT NULL,
  `price` decimal(16,2) DEFAULT NULL,
  `quantity` int(12) DEFAULT NULL,
  `sum` decimal(16,2) DEFAULT NULL,
  `purchase_date` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '购买日期',
  `note` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='购买信息表';

-- ----------------------------
-- Table structure for book
-- ----------------------------
DROP TABLE IF EXISTS `book`;
CREATE TABLE `book` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='book_core测试solr';

-- ----------------------------
-- Records of book
-- ----------------------------
INSERT INTO `book` VALUES ('1', 'SpringCloud');
INSERT INTO `book` VALUES ('2', 'SpringBoot');
INSERT INTO `book` VALUES ('3', 'JAVA NIO');
INSERT INTO `book` VALUES ('4', 'JVM');
INSERT INTO `book` VALUES ('5', 'cumpter network');
INSERT INTO `book` VALUES ('6', 'data structure');

-- ----------------------------
-- Table structure for city
-- ----------------------------
DROP TABLE IF EXISTS `city`;
CREATE TABLE `city` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `cityname` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `pid` int(12) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='solr测试表';

-- ----------------------------
-- Records of city
-- ----------------------------
INSERT INTO `city` VALUES ('1', '广州', '1');
INSERT INTO `city` VALUES ('2', '深圳', '2');
INSERT INTO `city` VALUES ('3', '佛山', '3');
INSERT INTO `city` VALUES ('4', '上海', '4');
INSERT INTO `city` VALUES ('5', '南京', '5');
INSERT INTO `city` VALUES ('6', '杭州', '6');
INSERT INTO `city` VALUES ('7', '惠州', '6');


