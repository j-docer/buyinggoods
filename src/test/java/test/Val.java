package test;


@Deprecated
public class Val<T> {
    T v;

    public void set(T v) {
        this.v = v;
    }

    public T get() {
        return v;
    }
}
