package test;

import org.junit.runner.RunWith;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import study.pj.buyingGoods.Application;


// 单元测试类
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public abstract class AbstractTest {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
        System.out.println("wancheg 测试类的加载");
    }
}
