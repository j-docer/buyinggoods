package test;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashSet;

// 测试ThreadLocal
// 使用ThreadLocal收集的数据，即对数据的同步。

@RestController
@Deprecated
public class TestControlelr {

    // 对象的引用，其他的内容还是可以修改在set其中的值。
    static HashSet<Val<Integer>> set = new HashSet<>();

    synchronized static void addSet(Val<Integer> v) {
        set.add(v);
    }

    static ThreadLocal<Val<Integer>> c = ThreadLocal.withInitial(() -> {
        Val<Integer> a = new Val<Integer>();
        a.set(0);
        addSet(a);
//            set.add(a);
        return a;
    });

    // 排队，加锁的时间加长。
    // 使用线程池的时候，加锁synchronized让时间都变慢
    public void _add() throws InterruptedException {
        Thread.sleep(100);
        Val<Integer> v = c.get();
        v.set(v.get() + 1);
    }

    // 数据流，将流的计算统计加上去。
    @RequestMapping("/start")
    public Integer start() {
        return set.stream().map(x -> x.get()).reduce((a, x) -> a + x).get();

    }

    @RequestMapping("/add")
    public Integer add() throws InterruptedException {
        _add();
        return 1;
    }

}
