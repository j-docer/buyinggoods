package study.pj.buyingGoods.solr.service.impl;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.common.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import study.pj.buyingGoods.solr.model.Book;
import study.pj.buyingGoods.solr.service.SolrService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


@Service
public class SolrServiceImpl implements SolrService {
    @Autowired
    SolrClient solrClient;

    @Override
    public void add(Book book) {
        SolrInputDocument document = new SolrInputDocument();
        document.setField("id", book.getId());
        document.setField("description", book.getDescription());
        try {
            solrClient.add(document);
            solrClient.commit();
        } catch (SolrServerException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(String query) {
        try {
            solrClient.deleteByQuery(query);
            solrClient.commit();
        } catch (SolrServerException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Book update(Book book) {
        try {
            solrClient.addBean(book);
            solrClient.commit();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SolrServerException e) {
            e.printStackTrace();
        }
        return book;
    }

    @Override
    public List<Book> queryAll(String description) {
        List<Book> bookList = new ArrayList<Book>();
        SolrQuery solrQuery = new SolrQuery();
        if (StringUtils.isEmpty(description)) {
            solrQuery.setQuery("*:*");
        } else {
            solrQuery.setQuery("description:" + description);
        }

        try {
            QueryResponse queryResponse = solrClient.query(solrQuery);
            if (queryResponse != null) {
                bookList = queryResponse.getBeans(Book.class);
            }
        } catch (SolrServerException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bookList;
    }
}
