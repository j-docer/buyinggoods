package study.pj.buyingGoods.solr.service;

import study.pj.buyingGoods.solr.model.Book;

import java.util.List;

public interface SolrService {
    void add(Book book);
    void delete(String query);
    Book update(Book book);
    List<Book> queryAll(String description);
}
