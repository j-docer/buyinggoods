package study.pj.buyingGoods.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import study.pj.buyingGoods.service.PurchaseService;

import java.util.concurrent.atomic.AtomicInteger;


@RestController
@Api(value = "PurchaseController", description = "购买的控制器")
public class PurchaseController {

    static Logger logger = LoggerFactory.getLogger(PurchaseController.class);

    @Autowired
    private PurchaseService purchaseService;


    private static AtomicInteger requestPurCount = new AtomicInteger(0);


    @GetMapping("/testIn")
    @ApiOperation(value = "testIn", notes = "测试系统是否启动成功")
    @ResponseBody
    public String testIn() {
        return "ok";
    }

    @GetMapping("/testPage")
    @ApiOperation(value = "testPage", notes = "测试页面会调用购买的信息！")
    public ModelAndView testPage() {
        ModelAndView mv = new ModelAndView("test");
        logger.info("开始抢购！");
        return mv;
    }

    @PostMapping("/purchase")
    public Result purchase(long userId, long productId, Integer quantity) {
//      ReqSum++;
        requestPurCount.incrementAndGet();
        logger.info("进行抢购！");

        // 使用乐观锁
//        boolean success = purchaseService.purchase(userId, productId, quantity);

        // 使用redis
        boolean success = purchaseService.purchaseRedis(userId, productId, quantity);

        String message = success ? "抢购成功" : "抢购失败";
        Result result = new Result(success, message);
        logger.info("购买商品-" + result);
//        System.out.println("抢购累计ReqSum-" + ReqSum);
        logger.info("抢购累计次数-" + requestPurCount);
        return result;
    }

    @Setter
    @Getter
    class Result {
        private boolean success = false;
        private String message = null;

        public Result() {
        }

        public Result(boolean success, String message) {
            this.message = message;
            this.success = success;
        }
    }


}
