package study.pj.buyingGoods.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import study.pj.buyingGoods.model.Msg;

@Controller
public class HomeController {
    static Logger logger = LoggerFactory.getLogger(PurchaseController.class);

    @RequestMapping("/")
    public String index(Model model) {
        logger.info("home page!");
        Msg msg = new Msg("测试标题", "测试内容", "额外信息，只对管理员显示");
        model.addAttribute("msg", msg);
        return "home";
    }

    @RequestMapping("/testHome")
    @ResponseBody
    public String testHome() {
        return "home is access!";
    }

}
