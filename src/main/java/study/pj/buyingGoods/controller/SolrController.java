package study.pj.buyingGoods.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import study.pj.buyingGoods.solr.model.Book;
import study.pj.buyingGoods.solr.service.SolrService;

import java.util.List;

@RestController
public class SolrController {
    @Autowired
    private SolrService solrService;

    @RequestMapping("/queryAll")
    public List<Book> queryAll(String description) {
        return solrService.queryAll(description);
    }
}
