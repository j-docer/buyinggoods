package study.pj.buyingGoods.config;

import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;


/**
 * 实现开启异步的实现处理。
 */
@Configuration
// 表示开启Spring的异步，这样就可以使用@Async驱动Spring使用异步，但是异步需要提供线程池。
// 因此当一个方法被标注为@Async的时候，Spring就会通过线程池的空闲线程去运行这个方法。
@EnableAsync
public class Asynconfig implements AsyncConfigurer {
    @Override
    public Executor getAsyncExecutor() {

        // 定义线程池
        ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();

        // 核心线程数
        taskExecutor.setCorePoolSize(10);
        // 线城池最大数量
        taskExecutor.setMaxPoolSize(30);
        // 线程队列最大线程数
        taskExecutor.setQueueCapacity(2000);
        // 初始化
        taskExecutor.initialize();
        return taskExecutor;
    }

    @Override
    public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
        return null;
    }
}
