package study.pj.buyingGoods.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundListOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import study.pj.buyingGoods.model.PurchaseRecordPo;
import study.pj.buyingGoods.service.PurchaseService;
import study.pj.buyingGoods.service.TaskService;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class TaskServiceImpl implements TaskService {

    static Logger logger = LoggerFactory.getLogger(TaskServiceImpl.class);

    @Autowired
    private StringRedisTemplate stringRedisTemplate = null;

    @Autowired
    private PurchaseService purchaseService = null;

    private static final String PRODUCT_SCHEDULE_SET = "product_schedule_set";
    private static final String PURCHASE_PRODUCT_LIST = "purchase_list_";

    // 每次取出1000条（数量不限），避免一次取出消耗太多内存
    private static final int ONE_TIME_SIZE = 1000;


    @Override
    // 每天1点定时拉取数据出来入库保存
//    @Scheduled(cron = "0 0 1 * * ?")
    // 下面用于测试配置，每2分钟执行一次
    @Scheduled(fixedRate = 1000 * 120)
    public void purchaseTask() {
        logger.info("开始执行定时任务");
        Set<String> productIdList = stringRedisTemplate.opsForSet().members(PRODUCT_SCHEDULE_SET);

        List<PurchaseRecordPo> prpList = new ArrayList<>();
        for (String productIdStr : productIdList) {
            Long productId = Long.parseLong(productIdStr);
            String purchaseKey = PURCHASE_PRODUCT_LIST + productId;
            BoundListOperations<String, String> ops = stringRedisTemplate.boundListOps(purchaseKey);
            long size = stringRedisTemplate.opsForList().size(purchaseKey);
            Long times = size % ONE_TIME_SIZE == 0 ? size / ONE_TIME_SIZE : size / ONE_TIME_SIZE + 1;
            for (int i = 0; i < times; i++) {
                List<String> prList = null;
                if (i == 0) {
                    prList = ops.range(i * ONE_TIME_SIZE, (i + 1) * ONE_TIME_SIZE);
                } else {
                    prList = ops.range(i * ONE_TIME_SIZE + 1, (i + 1) * ONE_TIME_SIZE);
                }

                for (String prStr : prList) {
                    PurchaseRecordPo prp = this.createPurchaseRecord(productId, prStr);
                    prpList.add(prp);
                }
                try {
                    purchaseService.dealRedisPurchase(prpList);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                prpList.clear();
            }
            // 删除购买列表
            stringRedisTemplate.delete(purchaseKey);
            // 从商品集合中删除商品
            stringRedisTemplate.opsForSet().remove(PRODUCT_SCHEDULE_SET, productIdStr);
        }
        logger.info("定时任务结束");
    }

    private PurchaseRecordPo createPurchaseRecord(Long productId, String prstr) {
        String[] arr = prstr.split(",");
        Long userId = Long.parseLong(arr[0]);
        int quantity = Integer.parseInt(arr[1]);
        double sum = Double.valueOf(arr[2]);
        double price = Double.valueOf(arr[3]);
        Long time = Long.parseLong(arr[4]);
        Timestamp purchaseTime = new Timestamp(time);

        PurchaseRecordPo pr = new PurchaseRecordPo();
        pr.setProductId(productId);
        pr.setPurchasetime(purchaseTime);
        pr.setPrice(price);
        pr.setQuantity(quantity);
        pr.setSum(sum);
        pr.setUserId(userId);
        pr.setNote("购买日期，时间：" + purchaseTime.getTime());
        return pr;
    }
}
