package study.pj.buyingGoods.service.impl;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import study.pj.buyingGoods.service.AsyncService;

@Service
public class AsyncServiceImpl implements AsyncService {

    @Override
    @Async
    // 会使用配置好的线程池中的线程去处理它。
    public void generateReport() {
        // 打印异步线程名称
        System.out.println("线程的名称为：[" + Thread.currentThread().getName() + "]");
    }
}
