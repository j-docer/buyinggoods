package study.pj.buyingGoods.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import redis.clients.jedis.Jedis;
import study.pj.buyingGoods.mapper.ProductMapper;
import study.pj.buyingGoods.mapper.PurchaseRecordMapper;
import study.pj.buyingGoods.model.ProductPo;
import study.pj.buyingGoods.model.PurchaseRecordPo;
import study.pj.buyingGoods.service.PurchaseService;

import java.util.List;

/**
 * 使用时间戳和版本号的方法实现抢购会导致出现大量次失败的情况，
 * 可以使用redis来实现抢购的目的，
 * 利用redis单线程和LUA语言的运行的原子性操作
 */

@Service
public class PurchaseServiceImpl implements PurchaseService {

    static Logger logger = LoggerFactory.getLogger(PurchaseServiceImpl.class);

    @Autowired
    StringRedisTemplate stringRedisTemplate = null;

    String purchaseScript =
            // 先将产品编号保存到集合中
            "redis.call('sadd',KEYS[1],ARGV[2]) \n"
                    // 购买列表
                    + "local productPurchaseList = KEYS[2]..ARGV[2] \n"
                    // 用户编号
                    + "local userId= ARGV[1] \n"
                    // 产品键
                    + "local product='product_'..ARGV[2] \n"
                    // 购买数量
                    + "local quantity=tonumber(ARGV[3]) \n"
                    // 当前库存
                    + "local stock = tonumber(redis.call('hget',product,'stock'))\n"
                    // 价格
                    + "local price = tonumber(redis.call('hget',product,'price')) \n"
                    // 购买时间
                    + "local purchase_date =ARGV[4]\n"
                    // 库存不足，返回0
                    + "if stock < quantity then return 0 end \n"
                    // 减库存
                    + "stock=stock-quantity\n"
                    + "redis.call('hset',product,'stock',tostring(stock)) \n"
                    // 计算价格
                    + "local sum=price * quantity \n"
                    // 合并购买记录数据
                    + "local purchaseRecord= userId..','..quantity..','"
                    + "..sum..','..price..','..purchase_date \n"
                    // 将购买记录保存到list里
                    + "redis.call('rpush',productPurchaseList,purchaseRecord) \n"
                    // 返回成功
                    + "return 1 \n";

    // Redis购买记录集合前缀
    private static final String PURCHASE_PRODUCT_LIST = "purchase_list_";

    // 抢购商品集合
    private static final String PRODUCT_SCHEDULE_SET = "product_schedule_set";

    // 32位SHAL编码，第一次执行的时候先让Redis进行缓存脚本返回
    private String shal = null;


    @Autowired
    private ProductMapper productMapper;

    @Autowired
    private PurchaseRecordMapper purchaseRecordMapper;

    // 读已提交：问题：不可以重复读。
    @Transactional(isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    public boolean purchase(Long userId, Long productId, int quantity) {

        // 使用时间戳和版本号实现可重入的机制。
        long start = System.currentTimeMillis();

        // 也可以使用重入次数的方式来进行重入尝试
        // 尝试3次，重入，3次失败就回退。
//        for(int i=0 ;i<=3;i++)
//        {
//
//            ProductPo product = productMapper.getProduct(productId);
//            // 库存不足，直接返回
//            if (product.getStock() < quantity) {
//                return false;
//            }
//
//            // 在本次的线程的栈中保存版本号（即副存），扣减库存
//            // 这种实现思路缺点：会有大量的请求失败的情况，所以会出现，请求数大于库存，但是库存还有
//            // 数量剩余
//            int version = product.getVersion();
//            int result = productMapper.decreaseProduct(productId, quantity, version);
//            if (result == 0) {
//                // 失败了，重新尝试
//                continue;
//            }
//            PurchaseRecordPo pr = this.initPurchaseRecord(userId, product, quantity);
//            // 插入购买记录
//            purchaseRecordMapper.insertPurchaseRecord(pr);
//            return true;
//        }

        while (true) {
            long end = System.currentTimeMillis();

            // 小于等于100毫秒的时候，失败重新尝试。
            if (end - start > 100) {
                return false;
            }

            ProductPo product = productMapper.getProduct(productId);
            // 库存不足，直接返回
            if (product.getStock() < quantity) {
                logger.info("库存不足，抢购失败");
                return false;
            }

            // 在本次的线程的栈中保存版本号（即副存），扣减库存
            // 这种实现思路缺点：会有大量的请求失败的情况，所以会出现，请求数大于库存，但是库存还有
            // 数量剩余
            int version = product.getVersion();
            int result = productMapper.decreaseProduct(productId, quantity, version);
            if (result == 0) {
                // 失败了，重新尝试
                continue;
            }

            // 成功进行减库存了，可以执行抢购的记录           、
            PurchaseRecordPo pr = this.initPurchaseRecord(userId, product, quantity);
            // 插入购买记录
            logger.info("抢购成功！");
            purchaseRecordMapper.insertPurchaseRecord(pr);
            return true;
        }
    }

    @Override
    public boolean purchaseRedis(Long userId, Long productId, int quantity) {
        Long purchaseDate = System.currentTimeMillis();

        Jedis jedis = null;
        try {
            // 获取原始连接
            jedis = (Jedis) stringRedisTemplate.getConnectionFactory().getConnection().getNativeConnection();
            // 如果没有加载过，则先将脚本加载到Redis服务器，让其返回shal
            if (shal == null) {
                shal = jedis.scriptLoad(purchaseScript);
            }
            //执行脚本，返回结果
            Object res = jedis.evalsha(shal, 2, PRODUCT_SCHEDULE_SET, PURCHASE_PRODUCT_LIST
                    , userId + "", productId + "", quantity + "", purchaseDate + "");
            Long result = (Long) res;
            return result == 1;
        } finally {
            // 关闭jedis连接
            if (jedis != null && jedis.isConnected()) {
                jedis.close();
            }
        }


    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public boolean dealRedisPurchase(List<PurchaseRecordPo> prpList) {
        for(PurchaseRecordPo prp: prpList)
        {
            purchaseRecordMapper.insertPurchaseRecord(prp);
            productMapper.decreaseProducts(prp.getProductId(),prp.getQuantity());
        }
        return true;
    }

    //
    private PurchaseRecordPo initPurchaseRecord(Long userId, ProductPo product, int quantity) {
        PurchaseRecordPo pr = new PurchaseRecordPo();
        pr.setNote("购买日期，时间：" + System.currentTimeMillis());
        pr.setPrice(product.getPrice());
        pr.setProductId(product.getId());

        pr.setQuantity(quantity);
        double sum = product.getPrice() * quantity;
        pr.setSum(sum);
        pr.setUserId(userId);
        return pr;
    }
}
