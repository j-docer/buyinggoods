package study.pj.buyingGoods.service;

public interface TaskService {
    /**
     * 购买定时任务
     */
    public void purchaseTask();
}
