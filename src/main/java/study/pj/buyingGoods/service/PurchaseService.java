package study.pj.buyingGoods.service;

import study.pj.buyingGoods.model.PurchaseRecordPo;

import java.util.List;

public interface PurchaseService {
    boolean purchase(Long userId, Long productId, int quantity);

    boolean purchaseRedis(Long userId, Long productId, int quantity);

    boolean dealRedisPurchase(List<PurchaseRecordPo> prpList);
}
