package study.pj.buyingGoods.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;
import study.pj.buyingGoods.model.Role;

@Mapper
@Component
public interface RoleMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Role record);

    int insertSelective(Role record);

    Role selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Role record);

    int updateByPrimaryKey(Role record);

    String findPrivilege(Integer id);


}