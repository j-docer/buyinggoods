package study.pj.buyingGoods.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;
import study.pj.buyingGoods.model.ProductPo;

@Mapper
@Component
public interface ProductMapper {

    ProductPo getProduct(Long id);

    int decreaseProduct(@Param("id") long id, @Param("quantity") int quantity, @Param("version") int version);

    int decreaseProducts(@Param("id") long id, @Param("quantity") int quantity);
}
