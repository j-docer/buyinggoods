package study.pj.buyingGoods.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;
import study.pj.buyingGoods.model.User;

@Mapper
@Component
public interface UserMapper {
    int deleteByPrimaryKey(Long id);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    User findByUserName(String username);

}