package study.pj.buyingGoods.mapper;


import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;
import study.pj.buyingGoods.model.PurchaseRecordPo;


@Mapper
@Component
public interface PurchaseRecordMapper {
    int insertPurchaseRecord(PurchaseRecordPo pr);
}
