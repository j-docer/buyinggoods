<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>购买产品测试</title>
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js">
    </script>
</head>
<script type="text/javascript">

    <%-- 数量可以调整，浏览器支撑不住3万的请求数量，会崩溃  --%>
    <%-- 实现高并发的场景 实现5万人抢购3万数量的产品，看是否包含有超发的现象：数据库数据--%>
    for (var i = 1; i <= 3000; i++) {
        var params = {
            userId: 1,
            productId: 1,
            quantity: 1
        };
        // 通过POST后端请求参数
        $.post("./purchase", params,
            function (result) {
                // alert(result.message)
            });
    }

</script>
<body>
<h1>抢购产品测试</h1>
</body>
</html>
